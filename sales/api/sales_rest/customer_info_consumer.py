# https://learn-2.galvanize.com/cohorts/3189/blocks/1877/content_files/build/03-pub-sub/67-unmonolith-with-pub-sub.md

import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

from sales_rest.models import CustomerVO

# Consumer code in pub/sub to populate CustomerVO model in Sales microservice


# Declare a function to update the CustomerVO object (ch, method, properties, body)
def update_customer_vo_object(ch, method, properties, body):
    # content = load the json in body
    content = json.loads(body)

    import_href = content["href"]
    name = content["name"]
    address = content["address"]
    phone_number = content["phone_number"]
    is_vip = content["is_vip"]

    CustomerVO.objects.update_or_create(
        import_href=import_href,
        defaults={
            "name": name,
            "address": address,
            "phone_number": phone_number,
            "is_vip": is_vip,
        },
    )


# Based on the reference code at
#   https://github.com/rabbitmq/rabbitmq-tutorials/blob/master/python/receive_logs.py
# infinite loop
while True:
    # try
    print("IM INSIDE THE WHILE LOOP, CONSUMER FOR SALES Microservice!!!!!!!!!")
    try:
        # print("IM INSIDE THE TRY BLOCK IN THE CONSUMER!!!!!!!!!!!!!")

        # create the pika connection parameters
        parameters = pika.ConnectionParameters(host="rabbitmq")
        # create a blocking connection with the parameters
        connection = pika.BlockingConnection(parameters)
        # open a channel
        channel = connection.channel()
        # declare a fanout exchange named "customer_info"
        channel.exchange_declare(exchange="customer_info", exchange_type="fanout")
        # declare a randomly-named queue
        result = channel.queue_declare(queue="", exclusive=True)
        # get the queue name of the randomly-named queue
        queue_name = result.method.queue
        # bind the queue to the "customer_info" exchange
        channel.queue_bind(exchange="customer_info", queue=queue_name)
        # do a basic_consume for the queue name that calls function above
        channel.basic_consume(
            queue=queue_name,
            on_message_callback=update_customer_vo_object,
            auto_ack=True,
        )
        # tell the channel to start consuming
        channel.start_consuming()
    # except AMQPConnectionError
    except AMQPConnectionError:
        # print that it could not connect to RabbitMQ
        print("Could not connect to RabbitMQ")
        # have it sleep for a couple of seconds
        time.sleep(2.0)
