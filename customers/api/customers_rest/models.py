from django.db import models
from django.urls import reverse


class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=200)
    is_vip = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_customer", kwargs={"pk": self.pk})

    class Meta:
        ordering = ("name", "phone_number", "address")
