# Generated by Django 4.0.3 on 2022-05-19 06:36

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('address', models.CharField(max_length=200)),
                ('phone_number', models.CharField(max_length=200)),
                ('is_vip', models.BooleanField(default=False)),
            ],
            options={
                'ordering': ('name', 'phone_number', 'address'),
            },
        ),
    ]
