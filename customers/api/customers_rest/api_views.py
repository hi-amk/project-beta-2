from django.http import JsonResponse, HttpResponse
from common.json import ModelEncoder
from .models import Customer
from django.views.decorators.http import require_http_methods
import json
import pika

from django.db import IntegrityError


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["name", "address", "phone_number", "is_vip", "id"]


# Producer code in pub/sub to populate CustomerVO models in Sales and Service microservices
def send_customer_data(customer):
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.exchange_declare(exchange="customer_info", exchange_type="fanout")

    message = json.dumps(customer, cls=CustomerEncoder)
    channel.basic_publish(
        exchange="customer_info",
        routing_key="",
        body=message,
    )
    connection.close()


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse({"customers": customers}, encoder=CustomerEncoder)
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)

        # What the below lines achieve at a high level:
        # The content dictionary only has the 4 fields (name, address, phone_number, is_vip) at this point
        # but what the CustomerVO in Sales and Service microservices need is
        # the import_href IN ADDITION to the 4 fields above.
        # These import_href attribute that we need can be obtained once the data "goes thru" the CustomerEncoder.
        # So that's what we do! Detailed explanation, line by line below:

        # store an instance of the CustomerEncoder class in a variable "customerencoder_class_instance"
        customerencoder_class_instance = CustomerEncoder()
        # call the default method of the class on that "custoemrencoder_class_instance",
        # while passing in the Customer object stored in variable customer as an argument
        customer_with_href_thru_encoder = customerencoder_class_instance.default(
            customer
        )

        # then the variable "customer_with_href_thru_encoder" will now have all the fields that the encoder added for us!
        # print("CUSTOMERENCODERPRINTED", customer_with_href_thru_encoder)

        # below is the FAILED attempt code at achieving the code block above.
        # leaving it here for records, showing that it was attempted and that the below DOES NOT WORK.
        # content_with_href_json = JsonResponse(
        #     customer, encoder=CustomerEncoder, safe=False
        # )

        # content_with_href_python = json.loads(content_with_href_json)
        # print(
        #     "TEST OUTPUT HERE!!!! - This should be content w HREF",
        #     content_with_href_python,
        # )

        send_customer_data(customer_with_href_thru_encoder)

        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_customer(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer does not exist"}, status=404)
    elif request.method == "DELETE":
        try:
            count, _ = Customer.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except:
            return JsonResponse(
                {
                    "message": "Deletion of this Customer is not allowed! Because this Customer has a Sale Record associated to them."
                },
                status=400,
            )
    else:
        try:
            content = json.loads(request.body)
            # Below line is for purpose of catching DoesNotExist BEFORE
            # checking whether ID in request body matches ID in endpoint.
            # Say url pk is 99 (assume doesn't exist), and ID in request
            # body is 3. Then, it should return 404-"Does not exist" msg
            # instead of 400-"Updating customer ID is not allowed"
            customer = Customer.objects.get(id=pk)
            if "id" in content and content["id"] is not pk:
                return JsonResponse(
                    {"message": "Updating customer ID is not allowed!"}, status=400
                )
            Customer.objects.filter(id=pk).update(**content)
            customer = Customer.objects.get(id=pk)

            # when a change is made to a customer instance,
            # the CustomerVO instances in Sales and Service microservices
            # should also be updated (this is achiever thru pub/sub)
            customerencoder_class_instance = CustomerEncoder()
            customer_with_href_thru_encoder = customerencoder_class_instance.default(
                customer
            )

            send_customer_data(customer_with_href_thru_encoder)

            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=404)
