from django.urls import path
from .api_views import (
    api_list_customers,
    api_show_customer,
)


urlpatterns = [
    path("customers/", api_list_customers, name="api_list_customers"),
    path("customers/<int:pk>/", api_show_customer, name="api_show_customer"),
]
