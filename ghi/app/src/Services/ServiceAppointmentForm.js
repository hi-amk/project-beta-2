import React from 'react';

class ServiceAppointmentForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      automobiles: [],
      automobile: '',
      customers: [],
      customer: '',
      dateTime: '',
      technicians: [],
      technician: '',
      reason: '',
      submit: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleCustomerChange = this.handleCustomerChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const newState = {}
    newState[event.target.id] = event.target.value;
    this.setState(newState)
  }

  async handleCustomerChange(event) {
    const customerValue = event.target.value;
    this.setState({ customer: customerValue });
    this.setState({ automobile: '' });

    // Populates the "Choose an Automobile"
    // dropdown based on the autos the customer has purchased at dealership

    // const automobilesUrl = 'http://localhost:8100/api/automobiles/';
    // const automobilesResponse = await fetch(automobilesUrl);

    // if (automobilesResponse.ok) {
    //   const automobilesData = await automobilesResponse.json();

    //   const autosPurchasedByThisCustomer = []
    //   for (const auto of automobilesData.autos) {
    //     if
    //   }

    //   this.setState({ automobiles: automobilesData.autos });
    // }

    const saleRecordUrl = 'http://localhost:8090/api/salerecords/';
    const saleRecordsResponse = await fetch(saleRecordUrl);

    if (saleRecordsResponse.ok) {
      const saleRecordsData = await saleRecordsResponse.json();

      const vinsPurchasedByThisCustomer = [];
      for (const sale_record of saleRecordsData.sale_records) {
        if (sale_record.customer.import_href === this.state.customer)
          vinsPurchasedByThisCustomer.push(sale_record.automobile.vin);
      }

      // if there's at least 1 vin in the list of vinsPurchasedByThisCustomer
      if (vinsPurchasedByThisCustomer) {
        const automobilesUrl = 'http://localhost:8100/api/automobiles/';
        const automobilesResponse = await fetch(automobilesUrl);

        if (automobilesResponse.ok) {
          const automobilesData = await automobilesResponse.json();

          const autosPurchasedByThisCustomer = []

          for (const auto of automobilesData.autos) {
            if (vinsPurchasedByThisCustomer.includes(auto.vin)) {
              autosPurchasedByThisCustomer.push(auto);
            }
          }

          this.setState({ automobiles: autosPurchasedByThisCustomer })

          // if list of vinsPurchasedByThisCustomer is empty
        } else {
          this.setState({ automobiles: 'No Autos purchased by this Customer' })
        }
      }
    }
  }


  async componentDidMount() {

    // Populates the "Choose a Customer" dropdown
    const customersUrl = 'http://localhost:8070/api/customers/';
    const customersResponse = await fetch(customersUrl);

    if (customersResponse.ok) {
      const customersData = await customersResponse.json();
      this.setState({ customers: customersData.customers });
    }

    // Populates the "Choose an Automobile" with all autos for now
    const automobilesUrl = 'http://localhost:8100/api/automobiles/';
    const automobilesResponse = await fetch(automobilesUrl);

    if (automobilesResponse.ok) {
      const automobilesData = await automobilesResponse.json();
      this.setState({ automobiles: automobilesData.autos });
    }

    // Populates the "Choose a Technician" dropdown
    const techniciansUrl = "http://localhost:8080/api/technicians/";

    const techniciansResponse = await fetch(techniciansUrl);

    if (techniciansResponse.ok) {
      const techniciansData = await techniciansResponse.json();
      this.setState({ technicians: techniciansData.technicians })
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    data.date_time = data.dateTime
    delete data.dateTime
    delete data.automobiles
    delete data.customers
    delete data.technicians
    delete data.submit
    console.log(data)

    const serviceAppointmentsURL = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      },
    };
    const serviceAppointmentPostResponse = await fetch(serviceAppointmentsURL, fetchConfig);
    if (serviceAppointmentPostResponse.ok) {
      const newAppointment = await serviceAppointmentPostResponse.json();
      console.log(newAppointment)

      const cleared = {
        automobile: '',
        customer: '',
        dateTime: '',
        technician: '',
        reason: '',
        submit: true
      };
      this.setState(cleared);
    } else {
      const jsonErrorMessage = (await serviceAppointmentPostResponse.json()).message
      alert(jsonErrorMessage);
    }
  }

  render() {
    let formClasses = '';
    let alertClasses = 'alert alert-success d-none mb-0'

    if (this.state.successfulSubmit) {
      formClasses = 'd-none';
      alertClasses = 'alert alert-success mb-0'
    }
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Appointment</h1>
            <form onSubmit={this.handleSubmit} id="create-service-form" className='{formClasses}'>
              <div className="mb-3">
                <select onChange={this.handleCustomerChange} value={this.state.customer} required name="customer" id="customer" className="form-select">
                  <option value="">Choose a Customer</option>
                  {this.state.customers.map(customer => {
                    return (
                      <option key={customer.href} value={customer.href}>
                        {customer.name} (Phone #: {customer.phone_number})
                      </option>
                    )
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select onChange={this.handleChange} value={this.state.automobile} name="automobile" id="automobile" className="form-select">
                  <option value="">Choose an Automobile, if purchased here</option>
                  {this.state.automobiles.map(auto => {
                    return (
                      <option key={auto.id} value={auto.vin}>
                        {auto.vin} - {auto.year} {auto.color} {auto.model.manufacturer.name} {auto.model.name}
                      </option>
                    )
                  })}
                </select>
              </div>


              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.automobile} placeholder="Automobile VIN" type="text" id="automobile" className="form-control" />
                <label htmlFor="automobile">Please enter an Automobile VIN, if not purchased here</label>
              </div>


              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.dateTime} placeholder="Date and Time" type="datetime-local" id="dateTime" className="form-control" />
                <label htmlFor="dateTime">Date and Time</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleChange} value={this.state.technician} required className="form-select" id="technician">
                  <option value="">Choose a technician</option>
                  {this.state.technicians.map(technician => {
                    return (
                      <option key={technician.employee_number} value={technician.employee_number}>{technician.name}</option>
                    )
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.reason} placeholder="Reason" type="text" id="reason" className="form-control" />
                <label htmlFor="reason">Reason</label>
              </div>
              <button className="btn btn-primary">Create Appointment</button>
            </form>
            <div className={alertClasses} id="success-message">
              Appointment has been created!
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ServiceAppointmentForm;
