# CarCar2



## Design

As shown on the diagram below, there are 4 bounded contexts:
1) Inventory API
2) Customers API
3) Sales API
4) Service API

The Inventory API has 3 models: Manufacturer, Vehicle Model, and Automobile. The Automobile model includes data from Vehicle Model, which includes data from Manufacturer.

The Customers API has 1 model: Customer

The Sales API and the Service API both get the Customer data from the Customers microservice, and store it in their respective CustomerVO model. This is achieved by a pub/sub, where the producer is in the Customers microservice, and the Sales and Service microservices both have their own consumer. This means that by the Customer model being shared between the Sales and the Service microservices, one customer can purchase a car from the dealership, and if/when that same customer makes service appointments at the dealership, that customer would be identified and recognized as the same customer.
It is important to note that the Customer model in the Customers microservice only has 3 attributes: name, address, phone_number. However, the CustomerVO models in both the Sales and Service microservices have 5 attributes: import_href and is_vip, in addition to the 3 mentioned earlier.

The Sales API and the Service API both have a poller that polls for the Automobile data from the Inventory API into the Sales API and Service API bonded contexts respectively. In both Sales and Service APIs, the data polled is stored in an AutomobileVO model, which is a value object.


![CarCarBoundedContexts](Module2_AssessmentProjectBeta_BoundedContexts_v2.png)



## Sales microservice

The Sales API has 4 models, a pub/sub, and a poller.
The 4 models are:
1) AutomobileVO (value object)
2) CustomerVO (value object)
3) SalesPerson (entity)
4) SalesRecord (value object)

### Models

The AutomobileVO model represents a car (with color, year, vin, model, and manufacturer as its attributes), and gets its data from the poller from the Inventory API (to be explained later).

The CustomerVO model represents a potential customer (with import_href, name, address, phone number, is_vip as attributes) that would purchase a car from the dealership (and also make service appointments).

The SalesPerson model represents a sales person (with name and employee number as attributes) that sells cars at the dealership.

The SaleRecord model represents a sale record for a car. As attributes, it has an automobile that is in the inventory, the sales person who sold the car, the customer that bought the car, and the price at which the car was purchased. Only one sale record can exist for a car in the inventory.

### Pub/Sub

The Sales API has a pub/sub consumer that gets the Customer data from the Customers microservice and stores it in the CustomerVO model in the Sales API. This is the integration point between the Customers API and the Sales API. When a customer instance is created, the Customers microservice produces a message/job containing the Customer data (with all 5 attributes: import_href, name, address, phone_number, is_vip), and pushes it to the consumer in the Sales API (as well as Service API) where the Customer data is stored in the CustomerVO model. Then the SaleRecord model can use the customer data to make sale record instances.

### Poller

The Sales API has a poller that gets the Automobile data from the Inventory API and stores it in the AutomobileVO model in the Sales API. This is the integration point between the Inventory API and the Sales API. Once the Automobile data from the Inventory API is available in the Sales API through the AutomobileVO model, then the SaleRecord model can use the automobile data (stored in the AutomobileVO) to make sale record instances.


### Application

The Graphical Human Interface is implemented with React. On the Sales side, the following pages are available:
* Create a Sales Person
* Create a Potential Customer
* Create a Sale Record
* Sales Records List
* Sales Person History


## Service microservice

The Sales API has 4 models, a pub/sub, and a poller.
The 4 models are:
1) AutomobileVO (value object)
2) CustomerVO (value object)
3) Technician (entity)
4) Appointment (value object)

### Models

The AutomobileVO model represents a car (with color, year, vin, model, and manufacturer as its attributes), and gets its data from the poller from the Inventory API (to be explained later).

The CustomerVO model represents a potential customer (with import_href, name, address, phone number, is_vip as attributes) that would make service appointments at the dealership (and also potentially purchase a car from the dealership).

The Technician model represents a technician (with name and employee number as attributes) that services cars through service appointments at the dealership.

The Appointment model represents a service appointment for a car. As attributes, it has a customer, an automobile that is in the inventory, the date and time of the service appointment, the reason for the appointment, the technician that will service the car, and an indicator whether the appointment has finished. Many service appointments can exist for a car in the inventory.

### Pub/Sub

The Service API has a pub/sub consumer that gets the Customer data from the Customers microservice and stores it in the CustomerVO model in the Service API. This is the integration point between the Customers API and the Service API. When a customer instance is created, the Customers microservice produces a message/job containing the Customer data (with all 5 attributes: import_href, name, address, phone_number, is_vip), and pushes it to the consumer in the Service API (as well as Sales API) where the Customer data is stored in the CustomerVO model. Then the Appointment model can use the customer data to make appointment instances.

### Poller

The Service API has a poller that gets the Automobile data from the Inventory API and stores it in the AutomobileVO model in the Service API. This is the integration point between the Inventory API and the Service API. Once the Automobile data from the Inventory API is available in the Service API through the AutomobileVO model, then the Appointment model can use the automobile data (stored in the AutomobileVO) to make appointment instances.


### Application

The Graphical Human Interface is implemented with React. On the Service side, the following pages are available:
* Create a Technician
* Create a Potential Customer
* Create an Appointment
* Appointments List
* Automobile Appointments History