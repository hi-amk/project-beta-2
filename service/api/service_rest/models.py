from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    model = models.CharField(max_length=50)
    manufacturer = models.CharField(max_length=50)

    def __str__(self):
        return (
            str(self.year)
            + " "
            + self.color
            + " "
            + self.manufacturer
            + " "
            + self.model
            + " - "
            + str(self.vin)
        )


class CustomerVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=200)
    is_vip = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name", "phone_number", "address")


class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.name + " - EmpNo: " + str(self.employee_number)

    # needs update: should be api_show_technician
    def get_api_url(self):
        return reverse("api_list_technicians", kwargs={"pk": self.id})

    class Meta:
        ordering = ("name", "employee_number")


class Appointment(models.Model):
    customer = models.ForeignKey(
        "CustomerVO",
        related_name="appointments",
        # when a customer is deleted from database,
        # corresponding appointment instances should be deleted
        on_delete=models.CASCADE,
    )

    automobile = models.ForeignKey(
        "AutomobileVO",
        related_name="appointments",
        # when an auto is deleted from database,
        # corresponding appointment instance should stay intact
        on_delete=models.PROTECT,
    )
    date_time = models.DateTimeField(null=True)
    reason = models.TextField(null=True)
    technician = models.ForeignKey(
        "Technician",
        related_name="appointments",
        # when a technician is deleted from database,
        # corresponding appointment instance should stay intact
        on_delete=models.PROTECT,
    )
    is_finished = models.BooleanField(default=False)

    class Meta:
        ordering = ("date_time", "technician", "customer", "automobile")
