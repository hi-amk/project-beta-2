from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

from common.json import ModelEncoder
from .models import AutomobileVO, CustomerVO, Technician, Appointment

# Create your views here.
class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["color", "year", "vin", "model", "manufacturer"]


class CustomerVOEncoder(ModelEncoder):
    model = CustomerVO
    properties = ["name", "address", "phone_number", "is_vip", "import_href"]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
    ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "automobile",
        "customer",
        "date_time",
        "technician",
        "reason",
        "is_finished",
    ]
    encoders = {
        "technician": TechnicianListEncoder(),
        "automobile": AutomobileVOEncoder(),
        "customer": CustomerVOEncoder(),
    }


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "automobile",
        "customer",
        "date_time",
        "technician",
        "reason",
    ]
    encoders = {
        "technician": TechnicianListEncoder(),
        "automobile": AutomobileVOEncoder(),
        "customer": CustomerVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()

        return JsonResponse({"technicians": technicians}, encoder=TechnicianListEncoder)
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        print(content)
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        services = Appointment.objects.all()
        return JsonResponse(
            {"services": services},
            encoder=AppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            technician_number = content["technician"]
            technician = Technician.objects.get(employee_number=technician_number)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid employee number"},
                status=400,
            )

        try:
            customer_href = content["customer"]
            customer = CustomerVO.objects.get(import_href=customer_href)
            content["customer"] = customer
        except CustomerVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer HREF"},
                status=400,
            )

        try:
            request_vin = content["automobile"]
            content["automobile"] = AutomobileVO.objects.get(vin=request_vin)
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "This auto does not exist. Check VIN."},
                status=404,
            )

        # if AutomobileVO.objects.filter(vin=request_vin).exists():
        #     content["is_vip"] = True
        # else:
        #     content["is_vip"] = False

        services = Appointment.objects.create(**content)
        return JsonResponse(
            services,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "PUT"])
def api_detail_appointment(request, pk):
    if request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        try:
            customer_href = content["customer"]
            customer = CustomerVO.objects.get(import_href=customer_href)
            content["customer"] = customer
        except CustomerVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer HREF"},
                status=400,
            )

        try:
            request_vin = content["automobile"]
            content["automobile"] = AutomobileVO.objects.get(vin=request_vin)
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "This auto does not exist. Check VIN."},
                status=404,
            )

        Appointment.objects.filter(id=pk).update(**content)
        service = Appointment.objects.get(id=pk)
        return JsonResponse(service, encoder=AppointmentDetailEncoder, safe=False)
