from django.contrib import admin

from .models import AutomobileVO, CustomerVO, Technician, Appointment


@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    pass


@admin.register(CustomerVO)
class CustomerVOAdmin(admin.ModelAdmin):
    pass


@admin.register(Technician)
class TechnicianAdmin(admin.ModelAdmin):
    pass


@admin.register(Appointment)
class AppointmentModel(admin.ModelAdmin):
    pass
